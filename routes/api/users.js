var express = require('express');
var mongoose = require('mongoose');
var User = require('../../models/user');
var Task = require('../../models/task');
router = express.Router();

router.get('/', function(req, res) {
  var option_where = eval("(" + req.query.where + ")");
  var option_sort = eval("(" + req.query.sort + ")");
  var option_select = eval("(" + req.query.select + ")");
  var option_skip = eval("(" + req.query.skip + ")");
  var option_limit = eval("(" + req.query.limit + ")");
  var option_count = eval("(" + req.query.count + ")");

  User.find(option_where).sort(option_sort).select(option_select).skip(option_skip).limit(option_limit)
      .then((users) => {
        if (option_count) {
          return res.status(200).send({message: "OK", data: users.length});
        }
        return res.status(200).send({message: "OK", data: users});
      })
      .catch((err) => {
        return res.status(500).send({message: "500 Sever Error", data: []});
      });
});

  router.post('/', function(req, res) {
    if(!req.body.name || !req.body.email){
      return res.status(400).send({message: "Users cannot be created (or updated) without a name or email", data:[]});
    }
    var createuser = {
      name: req.body.name,
      email: req.body.email,
      dateCreated: req.body.dateCreated,
      pendingTasks: req.body.pendingTasks
    }
    User.findOne({email: req.body.email}).then((user) => {
      if(user){
        return res.status(400).send({message:'Multiple users with the same email cannot exist.', data: []});
      }
      User.create(createuser).then((created) => {
        if (created.pendingTasks.length > 0){
          created.pendingTasks.map(tid =>{
            Task.findById(tid).then(task => {
              task.assignedUser = created._id;
              task.assignedUserName = created.name;
              task.save();
            });
          });
        }
        return res.status(201).send({message: "Successful created.", data: created});
      }).catch((err) => {
        return res.status(500).send({message: '500 Sever Error', data: []});
      });
    }).catch((err) => {
      return res.status(500).send({message: '500 Sever Error', data: []});
    });
  });

router.get('/:id', function(req, res){
  User.findById(req.params.id).then((user) => {
    if(user){
      return res.status(200).send({message:"OK", data: user});
    }
    return res.status(404).send({message:"User does not exist", data: []});
  }).catch((err) => {
    return res.status(404).send({message: 'User does not exist', data: []});
  })
});

router.put('/:id', function(req, res){
  if(!req.body.name || !req.body.email){
    return res.status(400).send({message: "Users cannot be created (or updated) without a name or email", data:[]});
  }
  User.findOne({email: req.body.email}).then((user) => {
    if(user){
      if(req.params.id != user.id){
        return res.status(400).send({message:'Multiple users with the same email cannot exist.', data: []});
      }
    }
  }).catch((err) => {
    return res.status(500).send({message: '500 Sever Error', data: []});
  });
  User.findById(req.params.id).then((user) => {
    if(!user){
      return res.status(404).send({message:"User does not exist", data: []});
    }
    user.name = req.body.name;
    user.email = req.body.email;
    if(req.body.dateCreated){
      user.dateCreated = req.body.dateCreated;
    }
    if(req.body.pendingTasks || req.body.pendingTasks == "" || req.body.pendingTasks == []){
      if(user.pendingTasks.length > 0){
        user.pendingTasks.map(tid =>{
          Task.findById(tid).then(task => {
            task.completed = true;
            task.save();
          });
        });
      }
      user.pendingTasks = req.body.pendingTasks;
      if(req.body.pendingTasks.length > 0){
        user.pendingTasks.map(tid =>{
          Task.findById(tid).then(task => {
            task.assignedUser = user._id;
            task.assignedUserName = user.name;
            task.save();
          });
        });
      }
    }
  user.save();
    return res.status(200).send({message: "Successful updated.", data: user});
  }).catch((err) => {
    return res.status(404).send({message: 'User does not exist..', data: []});
  })
});

router.delete('/:id', function(req, res){
  User.findByIdAndDelete(req.params.id).then((user) => {
    if(user){
      if (user.pendingTasks.length > 0){
        user.pendingTasks.map(tid =>{
          Task.findById(tid).then(task => {
            task.assignedUser = "";
            task.assignedUserName = "unassigned";
            task.save();
          });
        });
      }
      return res.status(200).send({message:"OK", data: user});
    }
    return res.status(404).send({message:"User does not exist", data: []});
  }).catch((err) => {
    return res.status(500).send({message: '500 Sever Error', data: []});
  })
});

module.exports = router;
