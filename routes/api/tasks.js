var express = require('express');
var mongoose = require('mongoose');
var User = require('../../models/user');
var Task = require('../../models/task');
router = express.Router();

router.get('/', function(req, res) {
  var option_where = eval("(" + req.query.where + ")");
  var option_sort = eval("(" + req.query.sort + ")");
  var option_select = eval("(" + req.query.select + ")");
  var option_skip = eval("(" + req.query.skip + ")");
  var option_limit = eval("(" + req.query.limit + ")");
  var option_count = eval("(" + req.query.count + ")");

  Task.find(option_where).sort(option_sort).select(option_select).skip(option_skip).limit(option_limit)
      .then((tasks) => {
        if (option_count) {
          return res.status(200).send({message: "OK", data: tasks.length});
        }
        return res.status(200).send({message: "OK", data: tasks});
      })
      .catch((err) => {
        return res.status(500).send({message: "500 Sever Error", data: []});
      });
});

router.post('/', function(req, res) {
  if(!req.body.name || !req.body.deadline){
    return res.status(400).send({message: "Tasks cannot be created (or updated) without a name or a deadline", data:[]});
  }
  var createtask = {
    name: req.body.name,
    description: req.body.description,
    deadline: req.body.deadline,
    assignedUser: req.body.assignedUser,
    assignedUserName: req.body.assignedUserName,
    completed: req.body.completed
  }
  var uid = req.body.assignedUser;
  Task.create(createtask).then((created) => {
    if(!created.completed && uid){
      User.findByIdAndUpdate(uid, {$push: {pendingTasks: created._id}}).then();
    }
    return res.status(201).send({message: "Successful created.", data: created});
  }).catch((err) => {
    return res.status(500).send({message: '500 Sever Error', data: []});
  });
});


router.get('/:id', function(req, res){
  Task.findById(req.params.id).then((task) => {
    if(task){
      return res.status(200).send({message:"OK", data: task});
    }
    return res.status(404).send({message:"Task does not exist", data: []});
  }).catch((err) => {
    return res.status(500).send({message: '500 Sever Error', data: []});
  })
});

router.put('/:id', function(req, res){
  if(!req.body.name || !req.body.deadline){
    return res.status(400).send({message: "Tasks cannot be created (or updated) without a name or a deadline", data:[]});
  }
  Task.findById(req.params.id).then((foundtask) => {
    if(foundtask){
      if(foundtask.assignedUser){
        User.findByIdAndUpdate(foundtask.assignedUser, {$pull: {pendingTasks: foundtask._id}}).then();
      }
      foundtask.name = req.body.name;
      if(req.body.description){
        foundtask.description = req.body.description;
      }
      foundtask.deadline = req.body.deadline;
      if(req.body.assignedUser){
        foundtask.assignedUser = req.body.assignedUser;
      }
      if(req.body.assignedUserName){
        foundtask.assignedUserName = req.body.assignedUserName;
      }
      if(req.body.completed != null){
        foundtask.completed = req.body.completed;
      }
      if((foundtask.completed == false) && foundtask.assignedUser){
        User.findByIdAndUpdate(foundtask.assignedUser, {$push: {pendingTasks: foundtask._id}}).then();
      }
      foundtask.save();
      return res.status(200).send({message:"OK", data: foundtask});
    } else {
      return res.status(404).send({message:"Task does not exist", data: []});
    }
  }).catch((err) => {
    return res.status(404).send({message:"Task does not exist", data: []});
  });
});

router.delete('/:id', function(req, res){
  Task.findByIdAndDelete(req.params.id).then((foundtask) => {
    if(foundtask){
      if(foundtask.assignedUser){
        User.findByIdAndUpdate(foundtask.assignedUser, {$pull: {pendingTasks: foundtask._id}}).then();
      }
      return res.status(200).send({message:"OK", data: foundtask});
    } else {
      return res.status(404).send({message:"Task does not exist", data: []});
    }
  }).catch((err) => {
    return res.status(404).send({message:"Task does not exist", data: []});
  });
});

module.exports = router;
